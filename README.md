## Weather web app
![Capture d’écran 2017-06-19 à 05.58.33.png](https://bitbucket.org/repo/yp8qL59/images/3558387549-Capture%20d%E2%80%99e%CC%81cran%202017-06-19%20a%CC%80%2005.58.33.png)

## Technologies

- AngularJS 1.5 (Compare to other versions of Angular or React, AngularJS 1 is very easy to setup and works better without a web server and http:// protocol)
- Sass
- Open Weather Map API

## Architecture

Because the App has to run simply by opening index.html (without a web server) Google Chrome will return CORS issue if we try to use angularJS routing/templating/include functions. To avoid this problem we are using inline templates in index.html but please consider that in production with a server it should use external files.

![Capture d’écran 2017-06-19 à 05.41.44.png](https://bitbucket.org/repo/yp8qL59/images/1705729996-Capture%20d%E2%80%99e%CC%81cran%202017-06-19%20a%CC%80%2005.41.44.png)

## Improvements

- Implementing unit tests
- Implementing loadings in components
- Handling errors
- Defining SASS variables
- Adjusting time format regarding locations
- Defining Browser/Devices Supports]
- Improve Responsive