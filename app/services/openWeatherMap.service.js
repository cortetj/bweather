//This Angular service makes call to the Open Weather Map API
app.service('openWeatherMapService', function ($http, OWM_API) {

	//Get current weather from city by name
	this.getWeatherFromCityByName = function (city, callback) {
	  	var url = OWM_API.url + 'weather?q='+city.name+','+city.country+'&appid=' + OWM_API.key
		$http({
			method: 'GET',
			url: url
		}).then(function successCallback(response) {
			callback(response.data);
		}, function errorCallback(response) {
			console.log(response);
		});
	};

	//Get forcast from city by name
	this.getForecastFromCityByName = function (city, callback) {
	  	var url = OWM_API.url + 'forecast?q='+city.name+','+city.country+'&appid=' + OWM_API.key
		$http({
			method: 'GET',
			url: url
		}).then(function successCallback(response) {
			callback(response.data);
		}, function errorCallback(response) {
			console.log(response);
		});
	};

});