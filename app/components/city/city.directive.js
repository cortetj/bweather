app.directive('cityCard', function(openWeatherMapService) {
	return {
		scope: {
		  city: '=city'
		},
		templateUrl: 'city.view.html',
		link: function($scope) {

			openWeatherMapService.getWeatherFromCityByName($scope.city, function(data) {
				$scope.city.weather = data.weather[0];
				$scope.city.temp = Math.round(data.main.temp - 273.15); //Kelvin to Celsius
				$scope.city.sunrise = new Date(data.sys.sunrise*1000); //Create Date from Unix Timestamp
				$scope.city.sunset = new Date(data.sys.sunset*1000); //Create Date from Unix Timestamp
			});	
		}
	};
});