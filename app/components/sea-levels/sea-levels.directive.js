app.directive('seaLevels', function(openWeatherMapService) {
	return {
		scope: {
		  city: '=city'
		},
		templateUrl: 'sea-levels.view.html',
		link: function($scope) {

			//Get the forcast from the city name
			openWeatherMapService.getForecastFromCityByName($scope.city, function(data) {
				var seaLevels = _.filter(data.list, function(obj){ return obj.dt_txt.indexOf('09:00:00') !== -1; });
				
				//Create an array of labels
				var labels = _.map(seaLevels, function(sl) {
					var dt = new Date(sl.dt*1000);
					var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
					return days[dt.getDay()];
				});
				$scope.labels = labels;
				
				//Create an array with the chart datas
				var data = _.map(seaLevels, function(sl) {
				   return sl.main.sea_level;
				});
				$scope.data = data;

				//setting Chart.js options
				$scope.colors = ["#50a8c6"];
				$scope.options = {
					scales:
					{
					    xAxes: [{
					        display: true,
					        ticks: {
	                            display: false,
	                            padding: 0
	                        },
	                        gridLines: {
	                        	color: 'rgba(255,255,255, 0.1)'
	                        }
					    }],
					    yAxes: [{
					        display: false,
					        ticks: {
	                            display: false,
	                            padding: 0
	                        }
					    }]
					}
				}

			});		
		}
	};
});