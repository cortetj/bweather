app.controller('seaLevelsController', function($scope) {
	
	$scope.hideSeaLevels = hideSeaLevels;

	//Close the sea levels overview
	function hideSeaLevels() {
		$scope.city.active = false;
	}
});