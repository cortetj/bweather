//Cities Controller
app.controller('citiesController', function($scope) {

	//list of loaded cities
  	$scope.cities = [
  	{
  		name: 'London',
  		country: 'UK',
  	},
  	{
  		name: 'Paris',
  		country: 'FR'
  	},
  	{
  		name: 'Rome',
  		country: 'IT'
  	},
  	{
  		name: 'Berlin',
  		country: 'DE'
  	},
  	{
  		name: 'Amsterdam',
  		country: 'NL'
  	}
  ];

  function addCity() {
  	//Add new city in the list
  }

  function deleteCity(city) {
  	//Delete a city from the list
  }
  
});