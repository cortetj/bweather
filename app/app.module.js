'use strict';

var app = angular.module('bweather', ['chart.js']);

//Open weather Map API infos
app.constant('OWM_API', {
	url: 'http://api.openweathermap.org/data/2.5/',
	key: 'c1456918bd1a9f4de77577a58318a79d'
});